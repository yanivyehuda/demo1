﻿$dbmversion = "V1"
$DBMRSSchema = "schema"
$jarPath = "C:\Users\Oleksandr Kosenko\source\Workspaces\DOP\Main\Source\DOP\DOP.Agent.Console\bin\Debug\DBmaestroAgent.jar"
$currentPath = (Resolve-Path .\).Path

$files = git diff --name-only --diff-filter=AM HEAD~1..HEAD -- Database\*.sql
Remove-Item -Path "Packages" -Recurse
New-Item -Path "Packages\$DBMVersion" -Name $DBMRSSchema -ItemType "directory"
$files | ForEach-Object {Copy-Item -Path $_ -Destination ".\Packages\$($DBMVersion)\$($DBMRSSCHEMA)" -verbose}
#Get-ChildItem -Path Packages\$dbmversion -Recurse | 
java -jar $jarPath -CreateManifestFile -PathToScriptsFolder "Packages\$($DBMVersion)"
Compress-Archive -Path Packages\$dbmversion\*  -DestinationPath .\$dbmversion.zip
