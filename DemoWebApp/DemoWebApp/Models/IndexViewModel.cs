﻿using System.Collections.Generic;

namespace DemoWebApp.Models
{
    public class IndexViewModel
    {
        public IndexViewModel()
        {
            Columns = new List<string>();
        }
        public List<string> Columns { get; set; }
        public string Message { get; set; }

    }
}