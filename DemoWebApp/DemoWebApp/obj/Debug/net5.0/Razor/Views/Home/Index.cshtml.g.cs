#pragma checksum "C:\Users\Oleksandr Kosenko\source\repos\gitlab\demo1\DemoWebApp\DemoWebApp\Views\Home\Index.cshtml" "{ff1816ec-aa5e-4d10-87f7-6f4963833460}" "5a83b0fcc9715a40e064f90f59e424ddbc8cf681"
// <auto-generated/>
#pragma warning disable 1591
[assembly: global::Microsoft.AspNetCore.Razor.Hosting.RazorCompiledItemAttribute(typeof(AspNetCore.Views_Home_Index), @"mvc.1.0.view", @"/Views/Home/Index.cshtml")]
namespace AspNetCore
{
    #line hidden
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.AspNetCore.Mvc.Rendering;
    using Microsoft.AspNetCore.Mvc.ViewFeatures;
#nullable restore
#line 1 "C:\Users\Oleksandr Kosenko\source\repos\gitlab\demo1\DemoWebApp\DemoWebApp\Views\_ViewImports.cshtml"
using DemoWebApp;

#line default
#line hidden
#nullable disable
#nullable restore
#line 2 "C:\Users\Oleksandr Kosenko\source\repos\gitlab\demo1\DemoWebApp\DemoWebApp\Views\_ViewImports.cshtml"
using DemoWebApp.Models;

#line default
#line hidden
#nullable disable
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"5a83b0fcc9715a40e064f90f59e424ddbc8cf681", @"/Views/Home/Index.cshtml")]
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"c9a15372e2e8dfa4a236842b4f1a10037cfbf3b7", @"/Views/_ViewImports.cshtml")]
    public class Views_Home_Index : global::Microsoft.AspNetCore.Mvc.Razor.RazorPage<IndexViewModel>
    {
        #pragma warning disable 1998
        public async override global::System.Threading.Tasks.Task ExecuteAsync()
        {
#nullable restore
#line 2 "C:\Users\Oleksandr Kosenko\source\repos\gitlab\demo1\DemoWebApp\DemoWebApp\Views\Home\Index.cshtml"
  
    ViewData["Title"] = "Home Page";

#line default
#line hidden
#nullable disable
            WriteLiteral("\r\n<div class=\"text-center\">\r\n    <h1 class=\"display-4\">Welcome, your message is:</h1>\r\n    <h3>");
#nullable restore
#line 8 "C:\Users\Oleksandr Kosenko\source\repos\gitlab\demo1\DemoWebApp\DemoWebApp\Views\Home\Index.cshtml"
   Write(Model.Message);

#line default
#line hidden
#nullable disable
            WriteLiteral("</h3>\r\n");
#nullable restore
#line 9 "C:\Users\Oleksandr Kosenko\source\repos\gitlab\demo1\DemoWebApp\DemoWebApp\Views\Home\Index.cshtml"
     if (Model.Columns.Any())
    {

#line default
#line hidden
#nullable disable
            WriteLiteral("        <h4>Columns in table demo_table:</h4>\r\n        <ul>\r\n");
#nullable restore
#line 13 "C:\Users\Oleksandr Kosenko\source\repos\gitlab\demo1\DemoWebApp\DemoWebApp\Views\Home\Index.cshtml"
             foreach (var col in Model.Columns)
            {

#line default
#line hidden
#nullable disable
            WriteLiteral("                <li>");
#nullable restore
#line 15 "C:\Users\Oleksandr Kosenko\source\repos\gitlab\demo1\DemoWebApp\DemoWebApp\Views\Home\Index.cshtml"
               Write(col);

#line default
#line hidden
#nullable disable
            WriteLiteral("</li>\r\n");
#nullable restore
#line 16 "C:\Users\Oleksandr Kosenko\source\repos\gitlab\demo1\DemoWebApp\DemoWebApp\Views\Home\Index.cshtml"
            }

#line default
#line hidden
#nullable disable
            WriteLiteral("        </ul>\r\n");
#nullable restore
#line 18 "C:\Users\Oleksandr Kosenko\source\repos\gitlab\demo1\DemoWebApp\DemoWebApp\Views\Home\Index.cshtml"
    }
    else
    {

#line default
#line hidden
#nullable disable
            WriteLiteral("        <h4>Looks like table demo_table does not exist!</h4>\r\n");
#nullable restore
#line 22 "C:\Users\Oleksandr Kosenko\source\repos\gitlab\demo1\DemoWebApp\DemoWebApp\Views\Home\Index.cshtml"

    }

#line default
#line hidden
#nullable disable
            WriteLiteral("</div>\r\n");
        }
        #pragma warning restore 1998
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.ViewFeatures.IModelExpressionProvider ModelExpressionProvider { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IUrlHelper Url { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IViewComponentHelper Component { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IJsonHelper Json { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IHtmlHelper<IndexViewModel> Html { get; private set; }
    }
}
#pragma warning restore 1591
