﻿using DemoWebApp.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using MySql.Data.MySqlClient;

namespace DemoWebApp.Controllers
{
    public class HomeController : Controller
    {
        private readonly ILogger<HomeController> _logger;

        public HomeController(ILogger<HomeController> logger)
        {
            _logger = logger;
        }

        public IActionResult Index()
        {
            using (MySqlConnection dbConn = new MySqlConnection("server=localhost;database=demoqa1;uid=root;password=1q2w3e4R"))
            using (MySqlCommand dbCmd = dbConn.CreateCommand())
            {
                dbCmd.CommandText =
                    "SELECT COLUMN_NAME FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_SCHEMA = 'demoqa1' AND TABLE_NAME = 'demo_table';";
                try
                {
                    dbConn.Open();
                    MySqlDataReader reader = dbCmd.ExecuteReader();
                    List<string> cols = new List<string>();
                    while (reader.Read())
                    {
                        cols.Add(reader[0].ToString());
                    }
                    return View(new IndexViewModel(){Message = "It works!", Columns = cols} );
                }
                catch (Exception erro)
                {
                    return View(new IndexViewModel(){Message = erro.ToString()});
                }
            }
        }

        public IActionResult Privacy()
        {
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
